# The-Battle-Cats-Unpack
A tool for unpack The Battle Cat APK

Can help you `Unpack the pack` `get the assets` of the APK you want to unpack
<sub> ~~you need to know the passcode~~ </sub>

## Required
* Java
    * If not installed, press the [link](https://javadl.oracle.com/webapps/download/AutoDL?BundleId=247917_0ae14417abb444ebb02b9815e2103550) to download java

## Installation
1. Press the [link](https://github.com/69you/The-Battle-Cats-Unpack/archive/refs/heads/master.zip) to download the zip and unzip it 
2. Download the the lastest version zip from the [release](https://github.com/69you/The-Battle-Cats-Unpack/releases) page
3. type `git clone https://github.com/69you/The-Battle-Cats-Unpack.git` in the terminal

## Steps
1. Create a file name `.env` and put the passcode of `.list` and `.pack`  
```
LIST=...
PACK=...
```
![.env](https://cdn.discordapp.com/attachments/1056286660649824348/1072800787811549244/image.png)



2. Put the APK of the battle cat in the folder named `APK`
![APK in the folder](https://cdn.discordapp.com/attachments/1056286660649824348/1072801231342415933/image.png)



3. Start Unpack
* Run the `The Battle Cats Unpack.exe` to start unpack the APK
or
* type `python unpack.py` in terminal


4. Finally you will get the assets of the APK
