import os, shutil, subprocess
from dotenv import load_dotenv
from io import BufferedReader
from Crypto.Cipher import AES
from Crypto.Util.Padding import pad
from tqdm import tqdm
load_dotenv()
#decode list pack
def split(file_path, start_byte:int, arrange:int): 
	with open(file_path, 'rb') as file:
		file.seek(start_byte)
		chunk = file.read(arrange-1)
		return chunk
        
# if os.path.exists('./APK/blank.txt'):
#     os.remove('./APK/blank.txt')
# # unpack apk
# if len(os.listdir('./APK')) == 0:
#     print('No apk Exist')
# elif len(os.listdir('./APK')) != 1:
#     print('Please put only a APK file inside')
# elif not os.listdir('./APK')[0].endswith('.apk'):
#     print('Please put a APK file inside')
# else:
#     apk = os.listdir('./APK')[0]
#     path = os.path.join('./APK/{}'.format(apk))

#     shutil.copy(path,f'./pack/{apk}')
#     #subprocess.run(["apktool", "if", "framework-res.apk"], cwd="./pack", shell=True)
#     subprocess.run(["apktool", "d", apk], cwd="./pack", shell=True)
#     os.remove(f'./PACK/{apk}')
#     os.remove(f'./APK/{apk}')
#     shutil.move(f'./pack/{apk[:-4]}',f'./APK/{apk[:-4]}')
#     os.mkdir('./APK/LIST_PACK')
#     for root, dirs, files in os.walk(f"./APK/{apk[:-4]}"):
#         for file in files:
#             if file.endswith('.list') or file.endswith('.pack'):
#                 shutil.copy(os.path.join(root, file),'./APK/LIST_PACK')

#     if not os.path.exists("./APK/txt"):
#         os.makedirs(name="./APK/txt",mode=0o777)
    #decode list and create to txt
for _ in os.listdir('./APK/LIST_PACK'):
    if _.endswith('.list'):
        with open(f'./APK/LIST_PACK/{_}','rb') as in_list:
            list_reader = BufferedReader(in_list)
            list_data  = list_reader.read()
        list_decode = AES.new(bytes(os.getenv('LIST'),'utf-8'), AES.MODE_ECB)
        list_res = list_decode.decrypt(list_data)
        with open('./APK/txt/{}.txt'.format(_[:-5]),'wb') as w_txt:
            w_txt.write(list_res)
        exit()
        if not os.path.exists("./APK/assets/{}".format(_[:-5])):
            os.makedirs(name="./APK/assets/{}".format(_[:-5]),mode=0o777)
exit()
for _ in os.listdir('./APK/LIST_PACK'):
    if _.endswith('.pack'):
        with open ('./APK/txt/{}.txt'.format(_[:-5]),'r') as r_txt:
            lines = r_txt.readlines()
        count = 0

        for line in tqdm(lines, total=len(lines)-2, desc=_[:-5]):
            if count==0 or count==int(lines[0].strip())+1:
                pass
            else:
                name = line.strip().split(',')[0]
                start_byte = line.strip().split(',')[1]
                arrange = line.strip().split(',')[2]
                #print("Line {}, name {}, start_byte {}, end_byte {}".format(count,name,start_byte,end_byte))          
                chunk = split('./APK/LIST_PACK/{}'.format(_), int(start_byte), int(arrange))
                path = os.path.join("./APK/assets/{}".format(_[:-5]),"{}".format(name[:-4]))
                file = os.path.join("./APK/assets/{}".format(_[:-5]),"{}".format(name))

                with open(path, 'wb') as split_file:
                    split_file.write(chunk)
                #print("line {}, start_byte {}, end_byte{} ,size {}".format(int(count)+1,start_byte,int(start_byte)+int(arrange)-1,os.path.getsize(path)))

                with open(path,'rb') as in_pack:
                    pack_reader = BufferedReader(in_pack)
                    pack_data = pack_reader.read()

                pack_decode = AES.new(bytes(os.getenv('PACK'),'utf-8'),AES.MODE_ECB)
                pack_res = pack_decode.decrypt(pad(pack_data,16))
                with open(file, 'wb') as output:
                    output.write(pack_res)
                os.remove(path)
            count += 1
shutil.rmtree('./APK/txt')
shutil.rmtree('./APK/LIST_PACK')
