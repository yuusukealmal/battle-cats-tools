import os, shutil, subprocess, time, zipfile
from dotenv import load_dotenv
from io import BufferedReader
from Crypto.Cipher import AES
from Crypto.Util.Padding import unpad
from tqdm import tqdm

def split(file_path, start_byte:int, arrange:int) -> bytes:
    '''Split Pack file into readable bytes''' 
    with open(file_path, 'rb') as file:
        return file.read()[start_byte:start_byte+arrange]

def red(text) -> str:
    return('\033[31m' + text + '\033[0m')
def yellow(text) -> str:
    return('\033[33m' + text + '\033[0m')
def litgt_blue(text) -> str:
    return('\033[36m' + text + '\033[0m')
def gray(text) -> str:
    return('\033[37m' + text + '\033[0m')

def check() -> bool:
    '''Check if there is a APK file inside the folder'''
    if len(os.listdir('./APK')) == 0:
        print(red('No apk Exist'))
        return False
    elif len(os.listdir('./APK')) != 1:
        print(red('Please put only a APK file inside'))
        return False
    elif not os.listdir('./APK')[0].endswith('.apk'):
        print(red('Please put a APK file inside'))
        return False
    else:
        return True

def check_version() -> bool:
    '''auto check game version ( cause by different decode way /not finish )'''
    if input(yellow('Is your game version higher then v8.3 ?') + gray('(y/n)')) == 'y':
        return True
    if input(yellow('Is your game version higher then v8.3 ?') + gray('(y/n)')) == 'n':
        return False
    else:
        print(red('Please enter a valid answer'))
        check_version()

def check_decrpyt_type() -> int:
    '''check if you want to decrypt local file or server file'''
    type = input(yellow('Do you want to decode local file or server file?') + gray('(local/server)'))
    if type == 'local':
        return 1
    if type == 'server':
        return 2
    else:
        print(red('Please enter a valid answer'))
        check_decrpyt_type()

def check_cc() -> str:
    '''check country code ( will cause different decode way )'''
    cc = input(yellow('please enter the country code: ') + gray('(jp/tw/en/kr)'))
    if (cc.lower() != 'jp') and (cc.lower() != 'tw') and (cc.lower() != 'en') and (cc.lower() != 'kr'):
        print(red('Please enter a valid country code'))
        exit()
    else:
        return cc

def decode_apk(path,apk):
    '''not sure if it is ok to skip'''
    shutil.copy(path,f'./pack/{apk}')
    # #subprocess.run(["apktool", "if", "framework-res.apk"], cwd="./pack", shell=True)
    subprocess.run(["apktool", "d", apk], cwd="./pack", shell=True)
    os.remove(f'./PACK/{apk}')
    os.remove(f'./APK/{apk}')
    shutil.move(f'./pack/{apk[:-4]}',f'./APK/{apk[:-4]}')

def unzip_apk(apk):
    '''rename apk to zip and unzip it'''
    os.rename(f'./APK/{apk}',f'./APK/{apk[:-4]}.zip')
    with zipfile.ZipFile(f'./APK/{apk[:-4]}.zip', 'r') as zip:
        zip.extractall(f'./APK/{apk[:-4]}')
    os.remove(f'./APK/{apk[:-4]}.zip')

def move_list_pack(apk):
    '''move list and pack file to a new folder'''
    os.mkdir('./APK/LIST_PACK')
    for root, dirs, files in os.walk(f"./APK/{apk[:-4]}"):
        for file in files:
            if file.endswith('.list') or file.endswith('.pack'):
                shutil.copy(os.path.join(root, file),'./APK/LIST_PACK')

def create_txt():
    '''create a new folder to store list -> file'''
    if not os.path.exists("./APK/txt"):
        os.makedirs(name="./APK/txt",mode=0o777)

def decode_list(item):
    '''decode list file to txt'''
    if item.endswith('.list'):
        with open(f'./APK/LIST_PACK/{item}','rb') as in_list:
            list_reader = BufferedReader(in_list)
            list_data  = list_reader.read()
        list_decode = AES.new(bytes(os.environ['LIST'],'utf-8'), AES.MODE_ECB)
        list_res = list_decode.decrypt(list_data)
        list_res = unpad(list_res, AES.block_size)
        with open('./APK/txt/{}.txt'.format(item[:-5]),'wb') as w_txt:
            w_txt.write(list_res)

def create_folder(item):
    '''create a new folder to store assets'''
    if not os.path.exists("./APK/assets/{}".format(item[:-5])):
        os.makedirs(name="./APK/assets/{}".format(item[:-5]),mode=0o777)

def read_txt(item):
    '''read txt file'''
    lines=[]
    if item.endswith('.pack'):
        with open ('./APK/txt/{}.txt'.format(item[:-5]),'r') as r_txt:
            lines = r_txt.readlines()
    return lines

def decrypt_type(thing:int):
    '''bool to local file or server file'''
    if thing == 1:
        pack = bytes.fromhex(os.environ[f"{cc.upper()}_PACK"])
        iv = bytes.fromhex(os.environ[f"{cc.upper()}_iv"])
        pack_decode = AES.new(pack,AES.MODE_CBC,iv)
        return pack_decode
    if thing == 2:
        pack = bytes(os.environ["PACK"],'utf-8')
        pack_decode = AES.new(pack,AES.MODE_ECB)
        return pack_decode

def delete_padding(pack_res :bytes) -> bytes:
    '''delete the unreadable padding at the end of file'''
    last =  pack_res[-1:]
    if last == b'\x00':
        return pack_res[:-1]
    elif last == b'\x01':
        return pack_res[:-1]
    elif last == b'\x02':
        return pack_res[:-2]
    elif last == b'\x03':
        return pack_res[:-3]
    elif last == b'\x04':
        return pack_res[:-4]
    elif last == b'\x05':
        return pack_res[:-5]
    elif last == b'\x06':
        return pack_res[:-6]
    elif last == b'\x07':
        return pack_res[:-7]
    elif last == b'\x08':
        return pack_res[:-8]
    elif last == b'\t':
        return pack_res[:-9]
    elif last == b'\n':
        superbytetest = last[-2:]
        if superbytetest == b'\n\n':
            return pack_res[:-10]
        else:
            return pack_res
    elif last == b'\x0b':
        return pack_res[:-11]
    elif last == b'\x0c':
        return pack_res[:-12]
    elif last == b'\r':
        return pack_res[:-13]
    elif last == b'\x0e':
        return pack_res[:-14]
    elif last == b'\x0f':
        return pack_res[:-15]
    elif last == b'\x10':
        return pack_res[:-16]
    else:
        return pack_res

def decrypt_pack(cc,type,item,lines,count):
    for line in tqdm(lines, total=len(lines)-2, desc=item[:-5]):
        try:
            if count!=0 or count!=int(lines[0].strip())+1:
                name = line.strip().split(',')[0]
                start_byte = line.strip().split(',')[1]
                arrange = line.strip().split(',')[2]       
                chunk = split('./APK/LIST_PACK/{}'.format(item), int(start_byte), int(arrange))
                file = os.path.join("./APK/assets/{}".format(item[:-5]),"{}".format(name))
                if item[:-5] == 'ImageDataLocal':
                    with open(file, 'wb') as split_file:
                        split_file.write(chunk)
                else:
                    decrypt = decrypt_type(type)
                    pack_res = decrypt.decrypt(chunk)
                    pack_res = delete_padding(pack_res)
                    with open(file, 'wb') as output:
                        output.write(pack_res)
            count += 1
        except IndexError:pass

def delete_unused():
    '''delete unused temp file'''
    shutil.rmtree('./APK/txt')
    shutil.rmtree('./APK/LIST_PACK')

'''__main__'''
if os.path.exists('./APK/blank.txt'):
    os.remove('./APK/blank.txt')
if check():
    load_dotenv()
    cc = check_cc()
    type = check_decrpyt_type()
    apk = os.listdir('./APK')[0]
    path = os.path.join('./APK/{}'.format(apk))
    # decode_apk(path,apk)
    # time.sleep(20)
    unzip_apk(apk)
    move_list_pack(apk)
    create_txt()
    for _ in os.listdir('./APK/LIST_PACK'):
        print(litgt_blue(_[:-5]))
        decode_list(_)
        create_folder(_)
        lines = read_txt(_)
        count = 0
        decrypt_pack(cc,type,_,lines,count)
else:
    pass
delete_unused()
